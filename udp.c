#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "udp.h"

int sender_init(socket_ctx_t *sender_ctx, char *ip_addr, int port)
{
    #ifdef _WIN32
    WSADATA wsaData;
    WSAStartup(0x0202, &wsaData);
    #endif

    //create a UDP socket
    sender_ctx->socket_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sender_ctx->socket_fd < 0)
    {
        #ifdef _WIN32
        fprintf(stderr, "Error creating UDP socket. Errno: %d\n", WSAGetLastError());
        #endif
        #ifdef __GNUC__
        fprintf(stderr, "Error creating UDP socket. Errno: %d\n", errno);
        #endif
        return -1;
    }

    //set the target port
    memset(&sender_ctx->sockaddr, 0, sizeof(sender_ctx->sockaddr));
    sender_ctx->sockaddr.sin_family = AF_INET;
    sender_ctx->sockaddr.sin_port = htons(port);

    //set the ip address
    #ifdef _WIN32
    if (inet_pton(AF_INET, ip_addr, &sender_ctx->sockaddr.sin_addr) == 0)
    #endif
    #ifdef __GNUC__
    if (inet_aton(ip_addr, &sender_ctx->sockaddr.sin_addr) == 0) 
    #endif
    {
        fprintf(stderr, "Error setting IP address\n");
        return -1;
    }

    return 0;
}

int sender_send(socket_ctx_t *sender_ctx, void *buffer, size_t buffer_len)
{
    //we only ever send orientation data that's less than the defined packet size
    if (buffer_len > UDP_PACKET_SIZE)
    {
        fprintf(stderr, "Packet is too large to send\n");
        return -1;
    }

    if (sendto(sender_ctx->socket_fd, (char *)buffer, UDP_PACKET_SIZE, 
        0, (const struct sockaddr *)&(sender_ctx->sockaddr), sizeof(sender_ctx->sockaddr)) < 0)
    {
        fprintf(stderr, "Error sending packet. errno: %d\n", errno);
        return -1;
    }

    return 0;
}

int sender_destroy(socket_ctx_t *sender_ctx)
{

    #ifdef _WIN32
    closesocket(sender_ctx->socket_fd);
    WSACleanup();
    #endif
    #ifdef __GNUC__
    close(sender_ctx->socket_fd);
    #endif

    //closing errors for an unknown reason
    //fprintf(stderr, "tryign to close socket: %d", sender_ctx->socket_fd);
    //if (close(sender_ctx->socket_fd) < 0);
    //{
    //    fprintf(stderr, "Error closing socket: %d\n", errno);
    //    return -1;
    //}

    return 0;
}

int receiver_init(socket_ctx_t *receiver_ctx, int port)
{
    #ifdef _WIN32
    WSADATA wsaData;
    WSAStartup(0x0202, &wsaData);
    #endif

    //create a UDP socket
    receiver_ctx->socket_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (receiver_ctx->socket_fd < 0)
    {
        fprintf(stderr, "Error creating UDP socket\n");
        return -1;
    }

    memset(&receiver_ctx->sockaddr, 0, sizeof(receiver_ctx->sockaddr));

    //bind to the port and receive on any address
    receiver_ctx->sockaddr.sin_family = AF_INET;
    receiver_ctx->sockaddr.sin_port = htons(port);
    receiver_ctx->sockaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(receiver_ctx->socket_fd, (const struct sockaddr *)&receiver_ctx->sockaddr, 
        sizeof(receiver_ctx->sockaddr)) < 0)
    {
        fprintf(stderr, "Error binding to port\n");
        return -1;
    }

    return 0;
}

int receiver_recv(socket_ctx_t *receiver_ctx, void *buffer)
{
    //receive a packet
    if ((recv(receiver_ctx->socket_fd, (char *)buffer, UDP_PACKET_SIZE, 0)) < 0)
    {
        fprintf(stderr, "Error receiving packet\n");
        return -1;
    }

    return 0;
}

int receiver_destroy(socket_ctx_t *receiver_ctx)
{
    #ifdef _WIN32
    closesocket(receiver_ctx->socket_fd);
    WSACleanup();
    #endif
    #ifdef __GNUC__
    close(receiver_ctx->socket_fd);
    #endif

    //closing errors for an unknown reason
    //if (close(receiver_ctx->socket_fd) < 0);
    //{
    //    fprintf(stderr, "Error closing socket\n");
    //    return -1;
    //}

    return 0;
}
