#ifndef __UDP_H__
#define __UDP_H__

#ifdef _WIN32
#include <WinSock2.h>
#include <WS2tcpip.h> //inet_pton
#pragma comment(lib, "ws2_32.lib")
#endif 

//unix
#ifdef __GNUC__
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/socket.h>
#endif

#define UDP_PACKET_SIZE 512 

//http://www.gamedev.net/topic/460430-client--server-udp-example// windows
//http://www.abc.se/~m6695/udp.html UNIX

typedef struct socket_ctx_s
{
    int socket_fd;
    struct sockaddr_in sockaddr;
} socket_ctx_t;

int sender_init(socket_ctx_t *sender_ctx, char *ip_addr, int port);
int sender_send(socket_ctx_t *sender_ctx, void *buffer, size_t buffer_len);
int sender_destroy(socket_ctx_t *sender_ctx);

int receiver_init(socket_ctx_t *receiver_ctx, int port);
int receiver_recv(socket_ctx_t *receiver_ctx, void *buffer);
int receiver_destroy(socket_ctx_t *receiver_ctx);

#endif
